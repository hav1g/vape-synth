# VapeSynth

### Local development

**VapeSynth**

1. `npm i -g vercel`
2. `vercel link` select `PlutusCorp`
3. `vercel env pull .env.local`
4. `yarn` in root
5. `yarn dev` to spin up local development server

**bc-graphql**

The BigCommerce GraphQL API lacks a bunch of functionality that's present in their full-fledge REST API. While they seem to be working on it, it doesn't appear as though they ship that frequently. Instead, we can bring the parts of the REST API we like over to our own GraphQL service.

1. Clone [bc-graphql](https://github.com/plutus-corp/bc-graphql)
2. `yarn` in the root
3. `yarn dev` will fire up a GraphQL server on `localhost:4000`
