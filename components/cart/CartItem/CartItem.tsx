import { ChangeEvent, useState, useEffect } from 'react'
import cn from 'classnames'
import Image from 'next/image'

import { Trash, Plus, Minus } from '@components/icons'
import { UpdateOperation } from '@lib/constants'
import { usePrice } from '@lib/hooks/usePrice'
import s from './CartItem.module.css'

interface Props {
  item: CartItem
  updateItem: (lineItem: Partial<CartItem>, operation: Operation) => void
  errors: StatusError[]
}

const CartItem = ({ item, updateItem, errors }: Props) => {
  const { price } = usePrice({
    amount: item?.extendedSalePrice ?? 0,
    base: item.extendedListPrice,
  })
  const [quantity, setQuantity] = useState(item.quantity)
  const [hasErrors, setHasErrors] = useState<boolean>(errors.length > 0)

  function handleQuantity(e: ChangeEvent<HTMLInputElement>) {
    const val = Number(e.target.value)

    if (Number.isInteger(val) && val >= 0) {
      setQuantity(val)
    }
  }

  function handleBlur() {
    const val = Number(quantity)

    if (val !== item.quantity) {
      updateItem(
        { quantity: val, product_id: item?.productId, id: item.id },
        UpdateOperation.UPDATE,
      )
    }
  }

  function increaseQuantity(n = 1) {
    const val = Number(quantity) + n

    if (Number.isInteger(val) && val >= 0) {
      setQuantity(val)
      updateItem(
        { quantity: val, product_id: item.productId, id: item.id },
        UpdateOperation.UPDATE,
      )
    }
  }

  function handleRemove() {
    updateItem({ id: item.id }, UpdateOperation.DELETE)
  }

  useEffect(() => {
    if (errors.length > 0) {
      setQuantity(quantity - 1)
      setHasErrors(true)
    } else {
      setHasErrors(false)
    }
  }, [errors])

  return (
    <li className={cn('flex flex-row space-x-8 py-8')}>
      <div className="w-16 h-16 bg-gray-200 relative overflow-hidden">
        <Image
          className={s.productImage}
          src={item?.imageUrl ?? ''}
          width={150}
          height={150}
          alt="Product Image"
          unoptimized
        />
      </div>
      <div className="flex-1 flex flex-col text-base">
        <div className="flex items-baseline mb-3">
          <span className="font-semibold text-lg cursor-pointer">
            {item.name}
          </span>
          <span className="ml-2 text-gray-400">{item.variantName}</span>
        </div>

        <div className="flex items-center">
          <button type="button" onClick={() => increaseQuantity(-1)}>
            <Minus width={18} height={18} />
          </button>
          <label>
            <input
              type="number"
              max={99}
              min={0}
              className={s.quantity}
              value={quantity}
              onChange={handleQuantity}
              onBlur={handleBlur}
              disabled={hasErrors}
            />
          </label>
          <button type="button" onClick={() => increaseQuantity(1)}>
            <Plus width={18} height={18} />
          </button>
        </div>
      </div>
      <div className="flex flex-col justify-between space-y-2 text-base">
        <span>{price}</span>
        <button className="flex justify-end" onClick={handleRemove}>
          <Trash />
        </button>
      </div>
    </li>
  )
}

export default CartItem
