import { useMutation } from '@apollo/client'
import { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import cn from 'classnames'

import { UserNav } from '@components/common'
import { Bag, Cross, Check } from '@components/icons'
import { Button } from '@components/ui'
import { useUI } from '@components/ui/context'
import { useCart } from '@lib/hooks/useCart'
import { usePrice } from '@lib/hooks/usePrice'
import { CREATE_CHECKOUT_REDIRECTS } from '@lib/mutations'

import CartItem from '../CartItem'
import s from './CartSidebarView.module.css'

const CartSidebarView: FC = () => {
  const router = useRouter()
  const { closeSidebar } = useUI()
  const { cart, updateItem, errors } = useCart()
  const isEmpty = (cart?.items?.length ?? 0) <= 0
  const { price: subTotal } = usePrice({
    amount: cart?.subTotal ?? 0,
  })
  const { price: total } = usePrice({
    amount: cart?.total ?? 0,
  })
  const [createCheckoutRedirects, { data, loading }] = useMutation(
    CREATE_CHECKOUT_REDIRECTS,
  )

  const items = cart?.items || []
  const error = null
  const success = null

  async function onCheckoutClick() {
    const cartId =
      typeof window !== 'undefined' && window.localStorage.getItem('cartId')

    await createCheckoutRedirects({ variables: { cartId } })
  }

  useEffect(() => {
    async function navigate() {
      try {
        await router.push(data?.createCheckoutRedirects?.checkoutUrl)
      } catch (e) {
        console.error(
          `Something went wrong with your cart ${JSON.stringify(e)}`,
        )
      }
    }

    if (!loading && data?.createCheckoutRedirects?.checkoutUrl) {
      navigate()
    }
  }, [data?.createCheckoutRedirects?.checkoutUrl, loading])

  return (
    <div
      className={cn(s.root, {
        [s.empty]: error,
        [s.empty]: success,
        [s.empty]: isEmpty,
      })}
    >
      <header className="px-4 pt-6 pb-4 sm:px-6">
        <div className="flex items-start justify-between space-x-3">
          <div className="h-7 flex items-center">
            <button
              onClick={closeSidebar}
              aria-label="Close panel"
              className="hover:text-gray-500 transition ease-in-out duration-150"
            >
              <Cross className="h-6 w-6" />
            </button>
          </div>
          <div className="space-y-1">
            <UserNav />
          </div>
        </div>
      </header>

      {isEmpty ? (
        <div className="flex-1 px-4 flex flex-col justify-center items-center">
          <span className="border border-dashed border-primary rounded-full flex items-center justify-center w-16 h-16 p-12 bg-gray-900 text-secondary">
            <Bag className="absolute" />
          </span>
          <h2 className="pt-6 text-2xl font-semibold tracking-wide text-center">
            Your cart is empty
          </h2>
        </div>
      ) : error ? (
        <div className="flex-1 px-4 flex flex-col justify-center items-center">
          <span className="border border-white rounded-full flex items-center justify-center w-16 h-16">
            <Cross width={24} height={24} />
          </span>
          <h2 className="pt-6 text-xl font-light text-center">
            We couldn’t process your purchase. Please check your card
            information and try again.
          </h2>
        </div>
      ) : success ? (
        <div className="flex-1 px-4 flex flex-col justify-center items-center">
          <span className="border border-white rounded-full flex items-center justify-center w-16 h-16">
            <Check />
          </span>
          <h2 className="pt-6 text-xl font-light text-center">
            Thank you for your order.
          </h2>
        </div>
      ) : (
        <>
          <div className="px-4 sm:px-6 flex-1">
            <h2 className="pt-1 pb-4 text-2xl leading-7 font-semibold text-base tracking-wide">
              Cart
            </h2>
            <ul className="py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y-2 sm:divide-gray-100 border-t-2 border-gray-100">
              {items.map((item) => (
                <CartItem
                  key={item.id}
                  item={item}
                  updateItem={updateItem}
                  errors={errors}
                />
              ))}
            </ul>
          </div>

          <div className="flex-shrink-0 px-4 py-5 sm:px-6">
            <div className="border-t-2 border-gray-100">
              <ul className="py-3">
                <li className="flex justify-between py-1">
                  <span>Subtotal</span>
                  <span>{subTotal}</span>
                </li>
                <li className="flex justify-between py-1">
                  <span>Taxes</span>
                  <span>Calculated at checkout</span>
                </li>
                <li className="flex justify-between py-1">
                  <span>Estimated Shipping</span>
                  <span className="font-semibold tracking-wide">FREE</span>
                </li>
              </ul>
              <div className="flex justify-between border-t-2 border-gray-100 py-3 font-semibold mb-10">
                <span>Total</span>
                <span>{total}</span>
              </div>
            </div>
            <Button
              Component="button"
              width="100%"
              onClick={onCheckoutClick}
              loading={loading}
              disabled={loading}
            >
              Proceed to Checkout
            </Button>
          </div>
        </>
      )}
    </div>
  )
}

export default CartSidebarView
