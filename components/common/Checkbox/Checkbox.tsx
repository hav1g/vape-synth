import { ChangeEvent, FC, useState, useEffect, useRef, RefObject } from 'react'
import cn from 'classnames'

import s from './Checkbox.module.css'

interface Props {
  item: string
  onChange: () => void
  checked: boolean | undefined
}

const Checkbox: FC<Props> = ({ item, onChange, checked }) => {
  const checkboxRef = useRef() as RefObject<HTMLInputElement>
  const [isChecked, setIsChecked] = useState<boolean | null>(checked || null)
  const classnames = cn(s.root, {
    [s['root--checked']]: isChecked,
    [s['root--not-checked']]: !isChecked,
  })

  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    setIsChecked(e.target.checked)

    if (onChange) {
      onChange()
    }
  }

  useEffect(() => {
    // If `isChecked` is set on mount, we need to also update the input ref's
    // .checked value to reflect that, otherwise we'll only have the checked
    // styles
    if (checkboxRef.current && isChecked) {
      checkboxRef.current.checked = true
    }
  }, [])

  return (
    <div className="flex items-center mb-2">
      <input
        id={item}
        className={classnames}
        type="checkbox"
        onChange={handleChange}
        ref={checkboxRef}
      />
      <label className="text-primary" htmlFor={item}>
        {item}
      </label>
    </div>
  )
}

export default Checkbox
