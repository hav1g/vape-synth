import Link from 'next/link'
import { FC, useState } from 'react'
import { Range } from 'rc-slider'
import cn from 'classnames'

import Checkbox from '@components/common/Checkbox'

import s from './Filter.module.css'

type Filter = {
  id: number
  displayName: string
  filters: Array<{ name: string; id: number }>
}

interface Props {
  filters: Filter[]
  updateFilters: (filter: { id: number }) => () => void
  priceRange: number[]
  updatePriceRange: (price: number[]) => void
  selectedFilters?: number[] | null
  currentPath?: string
}

const Filter: FC<Props> = ({
  filters,
  updateFilters,
  priceRange,
  updatePriceRange,
  currentPath,
  selectedFilters,
}) => {
  const [expandedFilters, setExpandedFilters] = useState<number[]>([])
  const section = (idx: number) =>
    cn('mb-4', {
      'border-b-2 border-gray-100': filters && idx !== filters.length - 1,
    })

  function handleExpandFilter(idx: number) {
    return () => {
      if (!expandedFilters.includes(idx)) {
        setExpandedFilters([...expandedFilters, idx])
      } else {
        const updated = expandedFilters.filter((i) => i !== idx)
        setExpandedFilters(updated)
      }
    }
  }

  return (
    <div className="bg-white rounded-md shadow-md">
      <div className="mb-4 border-b-2 border-gray-100 px-8 py-6">
        <div className="flex items-between mb-2">
          <h3 className="text-lg font-semibold">
            Price <span className="text-sm text-accents-6">USD</span>
          </h3>
        </div>
        <Range
          min={0}
          max={300}
          allowCross={false}
          defaultValue={priceRange}
          onChange={updatePriceRange}
        />
        <div className="flex justify-between mt-4">
          <div>
            <span className="text-sm text-accents-6 mr-1">Min</span>{' '}
            <span className="inline-flex border-b-2 border-gray-100 pr-2 font-semibold w-16">
              {priceRange[0]}
            </span>
          </div>
          <div>
            <span className="text-sm text-accents-6 mr-1">Max</span>{' '}
            <span className="inline-flex border-b-2 border-gray-100 pr-2 font-semibold w-16">
              {priceRange[1]}
            </span>
          </div>
        </div>
      </div>
      {filters?.map((c: Filter, idx: number) => (
        <div className={section(idx)} key={c.displayName}>
          <div className="px-8 pb-6">
            <div className="flex items-between mb-2">
              <h3 className="text-lg font-semibold">{c.displayName}</h3>
            </div>
            {filters[idx].filters
              .slice(
                0,
                expandedFilters.includes(idx)
                  ? filters[idx].filters.length - 1
                  : 4,
              )
              .map(({ name, id }: { id: number; name: string }) => (
                <Checkbox
                  item={name}
                  onChange={updateFilters({ id })}
                  key={`n:${name}--e:${id}`}
                  checked={(selectedFilters ?? []).includes(id)}
                />
              ))}
            {filters[idx].filters.length > 4 ? (
              <button onClick={handleExpandFilter(idx)}>
                <span className="text-gray-500 underline">
                  {expandedFilters.includes(idx) ? 'Less' : 'More'}
                </span>
              </button>
            ) : null}
          </div>
        </div>
      ))}
      <div className="flex items-center justify-center pb-6">
        <Link
          href={{
            pathname: currentPath,
            ...(selectedFilters
              ? {
                  query: {
                    categoriesIn: selectedFilters?.join(','),
                  },
                }
              : {}),
          }}
        >
          <a className={s.link}>Apply filters</a>
        </Link>
      </div>
    </div>
  )
}

export default Filter
