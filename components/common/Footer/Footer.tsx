import { FC } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import { useRouter } from 'next/router'
import type { Page } from '@bigcommerce/storefront-data-hooks/api/operations/get-all-pages'
import getSlug from '@lib/get-slug'
import { Logo, Container } from '@components/ui'

interface Props {
  className?: string
  children?: any
  pages?: Page[]
}

const LEGAL_PAGES = ['terms-of-use', 'shipping-returns', 'privacy-policy']

const Footer: FC<Props> = ({ className, pages }) => {
  const rootClassName = cn(className)

  return (
    <footer className={rootClassName}>
      <Container>
        <div className="grid grid-cols-1 lg:grid-cols-12 gap-8  py-12 text-secondary transition-colors duration-150">
          <div className="col-span-1 lg:col-span-2">
            <Link href="/">
              <a className="flex flex-initial items-center font-bold md:mr-24">
                <span className="rounded-full border border-gray-700 mr-2">
                  <Logo />
                </span>
                <span>VapeSynth</span>
              </a>
            </Link>
          </div>
          <div className="col-span-1 lg:col-span-2">
            <ul className="flex flex-initial flex-col md:flex-1">
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Home
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    E-liquids
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-span-1 lg:col-span-2">
            <ul className="flex flex-initial flex-col md:flex-1">
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/privacy-policy">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Privacy policy
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/age-policy">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Age policy
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/terms-of-use">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Terms of use
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/returns-and-exchanges">
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Returns & exchanges
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="py-12 flex flex-col md:flex-row justify-between items-center space-y-4">
          <div>
            <p className="text-sm text-accents-4 mb-6">
              Not for Sale for Minors - Products sold on this site may contain
              nicotine which is a highly addictive substance. California
              Proposition 65 - WARNING: This product can expose you to chemicals
              including nicotine, which is known to the State of California to
              cause birth defects or other reproductive harm. For more
              information, go to Proposition 65 Warnings Website. Products sold
              on this site is intended for adult smokers. You must be of legal
              smoking age in your territory to purchase products. Please consult
              your physician before use. E-Juice on our site may contain
              Propylene Glycol and/or Vegetable Glycerin, Nicotine and
              Flavorings. Our products may be poisonous if orally ingested.
              Products sold by Vape Synth are not smoking cessation products and
              have not been evaluated by the Food and Drug Administration, nor
              are they intended to treat, prevent or cure any disease or
              condition. For their protection, please keep out of reach of
              children and pets. Read our terms and conditions page before
              purchasing our products. Use All Products On This Site At Your Own
              Risk!
            </p>
            <span className="text-sm text-secondary">
              &copy; 2020 VapeSynth LLC Inc. All rights reserved.
            </span>
          </div>
        </div>
      </Container>
    </footer>
  )
}

export default Footer
