import { useLazyQuery } from '@apollo/client'
import type { Page } from '@bigcommerce/storefront-data-hooks/api/operations/get-all-pages'
import { CommerceProvider } from '@bigcommerce/storefront-data-hooks'
import { usePreventScroll } from '@react-aria/overlays'
import cn from 'classnames'
import dayjs from 'dayjs'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FC, useEffect } from 'react'

import { useUI } from '@components/ui/context'
import { Navbar, Footer, Menu } from '@components/common'
import { Sidebar, Button, Modal, LoadingDots } from '@components/ui'
import { CartSidebarView } from '@components/cart'

import { StatusCodes, UpdateOperation } from '@lib/constants'
import { useAcceptCookies } from '@lib/hooks/useAcceptCookies'
import { useCart } from '@lib/hooks/useCart'
import { useIamNotAChild } from '@lib/hooks/useIAmNotAChild'

import s from './Layout.module.css'
import { GET_CART } from '@lib/queries'

const Loading = () => (
  <div className="w-80 h-80 flex items-center text-center justify-center p-3">
    <LoadingDots />
  </div>
)

const dynamicProps = {
  loading: () => <Loading />,
}

const LoginView = dynamic(
  () => import('@components/auth/LoginView'),
  dynamicProps,
)
const SignUpView = dynamic(
  () => import('@components/auth/SignUpView'),
  dynamicProps,
)
const ForgotPassword = dynamic(
  () => import('@components/auth/ForgotPassword'),
  dynamicProps,
)

const AgeVerification = dynamic(
  () => import('@components/verification/AgeVerification'),
  dynamicProps,
)

const FeatureBar = dynamic(
  () => import('@components/common/FeatureBar'),
  dynamicProps,
)

interface Props {
  pageProps: {
    pages?: Page[]
  }
}

const Layout: FC<Props> = ({ children, pageProps }) => {
  const [getCart, { data, loading }] = useLazyQuery(GET_CART)
  const {
    displaySidebar,
    displayMenu,
    displayModal,
    closeSidebar,
    closeMenu,
    closeModal,
    modalView,
  } = useUI()
  const { createOrUpdateCart, resetCartStorage } = useCart()
  const { acceptedCookies, onAcceptCookies } = useAcceptCookies()
  const { displayAgeVerificationModal } = useIamNotAChild()
  const { locale = 'en-US' } = useRouter()
  const expiresAt =
    typeof window !== 'undefined' && window.localStorage.getItem('expires_at')
  const cartId =
    typeof window !== 'undefined' && window.localStorage.getItem('cartId')

  usePreventScroll({
    isDisabled: !(displaySidebar || displayModal),
  })

  useEffect(() => {
    displayAgeVerificationModal()

    if (cartId) {
      getCart({ variables: { cartId } })
    }

    if (expiresAt && dayjs(Date.now()).isAfter(expiresAt)) {
      debugger
      createOrUpdateCart({ operation: UpdateOperation.DELETE })
    }
  }, [])

  useEffect(() => {
    if (data?.getCart?.error?.code === StatusCodes.NOT_FOUND) {
      debugger
      resetCartStorage()
    }
  }, [data?.getCart?.error?.code])

  return (
    <CommerceProvider locale={locale}>
      <div className={cn(s.root)}>
        <Navbar />
        <main className="fit">{children}</main>
        <Footer pages={pageProps.pages} className="bg-accents-9" />

        <Sidebar open={displaySidebar} onClose={closeSidebar}>
          <CartSidebarView />
        </Sidebar>

        <Sidebar open={displayMenu} onClose={closeMenu}>
          <Menu />
        </Sidebar>

        <Modal open={displayModal} onClose={closeModal}>
          {modalView === 'LOGIN_VIEW' && <LoginView />}
          {modalView === 'SIGNUP_VIEW' && <SignUpView />}
          {modalView === 'FORGOT_VIEW' && <ForgotPassword />}
          {modalView === 'AGE_VERIFICATION' && <AgeVerification />}
        </Modal>

        <FeatureBar
          title="This site uses cookies to improve your experience. By clicking, you agree to our Privacy Policy."
          hide={acceptedCookies}
          action={
            <Button className="mx-5" onClick={onAcceptCookies}>
              Accept cookies
            </Button>
          }
        />
      </div>
    </CommerceProvider>
  )
}

export default Layout
