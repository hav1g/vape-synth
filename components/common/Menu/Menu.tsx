import Link from 'next/link'
import type { FC } from 'react'

import { UserNav } from '@components/common'
import { Cross } from '@components/icons'
import { useUI } from '@components/ui/context'
import s from './Menu.module.css'

const Menu: FC = () => {
  const { closeMenu } = useUI()

  return (
    <div>
      <header className="px-4 pt-6 pb-4 sm:px-6">
        <div className="flex items-start justify-between space-x-3">
          <div className="h-7 flex items-center">
            <button
              onClick={closeMenu}
              aria-label="Close panel"
              className="hover:text-gray-500 transition ease-in-out duration-150"
            >
              <Cross className="h-6 w-6" />
            </button>
          </div>
          <div className="space-y-1">
            <UserNav />
          </div>
        </div>
      </header>
      <nav className="flex flex-col w-64">
        <Link href="/e-liquids">
          <a className={s.link} onClick={closeMenu}>
            E-liquids
          </a>
        </Link>
        <Link href="/search">
          <a className={s.link} onClick={closeMenu}>
            Brands
          </a>
        </Link>
      </nav>
    </div>
  )
}

export default Menu
