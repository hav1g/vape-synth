import { FC, useState, useEffect } from 'react'
import Link from 'next/link'
import cn from 'classnames'
import throttle from 'lodash.throttle'

import { Container } from '@components/ui'
import { Searchbar, UserNav } from '@components/common'
import { Menu } from '@components/icons'
import { useUI } from '@components/ui/context'

import s from './Navbar.module.css'

const Navbar: FC = () => {
  const { toggleMenu } = useUI()
  const [hasScrolled, setHasScrolled] = useState(false)

  const handleScroll = () => {
    const offset = 0
    const { scrollTop } = document.documentElement
    const scrolled = scrollTop > offset
    setHasScrolled(scrolled)
  }

  useEffect(() => {
    document.addEventListener('scroll', throttle(handleScroll, 200))
    return () => {
      document.removeEventListener('scroll', handleScroll)
    }
  }, [handleScroll])

  return (
    <div className={cn(s.root, { 'shadow-magical': hasScrolled })}>
      <div className="h-1 bg-gradient-to-r from-purple-500 to-red-500" />
      <div>
        <Container>
          <div className="flex justify-between items-center flex-row py-6">
            <div className="flex lg:hidden">
              <button onClick={toggleMenu}>
                <Menu />
              </button>
            </div>
            <Link href="/">
              <a className="text-xl font-semibold" aria-label="Logo">
                VapeSynth
              </a>
            </Link>
            <div className="justify-center hidden lg:flex w-2/4">
              <Searchbar />
            </div>
            <div className="flex justify-end space-x-8">
              <UserNav />
            </div>
          </div>
        </Container>
      </div>

      <div className="bg-accents-8">
        <Container>
          <div className="flex flex-1 items-center py-4 md:py-6 relative">
            <nav className="space-x-32 hidden lg:block">
              <Link href="/e-liquids">
                <a className={s.link}>E-liquids</a>
              </Link>
              <Link href="/brands">
                <a className={s.link}>Brands</a>
              </Link>
            </nav>
          </div>
          <div className="flex pb-4 lg:px-6 lg:hidden">
            <Searchbar id="mobile-search" />
          </div>
        </Container>
      </div>
    </div>
  )
}

export default Navbar
