import { useQuery, DocumentNode } from '@apollo/client'
import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import cn from 'classnames'

import rangeMap from '@lib/range-map'
import { PRODUCTS } from '@lib/queries'
import { ROUTING_PARAMS } from '@lib/constants'
import { Filter } from '@components/common'
import { Card } from '@components/product'
import { Container, Grid, Pagination, Skeleton } from '@components/ui'
import { Filter as FilterIcon } from '@components/icons'

type NumberOrStringObject = { [k: string]: number | string }

interface Props {
  type: string
  categoriesIn: number[]
  filterQuery: {
    q: DocumentNode
    name: string
  }
}

export default function ProductListLayout({
  type,
  categoriesIn,
  filterQuery,
}: Props) {
  const router = useRouter()
  let currentPageNumber = 1
  if (router.query?.page) {
    currentPageNumber = Number(router.query?.page)
  }

  const { data, loading: productsLoading, fetchMore } = useQuery(PRODUCTS, {
    variables: {
      categoriesIn,
      offset: 0,
      limit: 12,
      type,
    },
  })
  const { data: productFilters, loading: productFiltersLoading } = useQuery(
    filterQuery.q,
  )
  const [filters, setFilters] = useState<number[]>([])
  const [priceRange, setPriceRange] = useState<number[]>([0, 300])
  const [filterExpanded, setFilterExpanded] = useState<boolean>(false)

  function updatePriceRange([min, max]: number[]) {
    setPriceRange([min, max])
  }

  function expandFilter() {
    setFilterExpanded(!filterExpanded)
  }

  function updateFilters({ id }: { id: number }) {
    return () => {
      if (filters.includes(id)) {
        const updatedIds = filters.filter((i: number) => i !== id)

        setFilters(updatedIds)
      } else {
        setFilters([...filters, id])
      }
    }
  }

  useEffect(() => {
    let variables
    if (router?.query?.categoriesIn) {
      const ids = (router.query.categoriesIn as string)
        .split(',')
        .map((n) => parseInt(n, 10))

      setFilters(ids)

      variables = {
        categoriesIn: ids,
        price: { min: priceRange[0], max: priceRange[1] },
        type,
      }

      fetchMore({ variables })
    } else {
      variables = {
        offset: (currentPageNumber - 1) * 12,
        limit: 12,
      }
      fetchMore({ variables })
    }
  }, [router?.query])

  const routingParams = {
    [ROUTING_PARAMS.PATH_NAME]: router.pathname,
    [ROUTING_PARAMS.CURRENT_PAGE_NUMBER]: currentPageNumber,
  }

  return (
    <Container>
      <div className="grid grid-cols-12 gap-4 mt-3 mb-20">
        <div className="col-span-12 md:hidden">
          <div
            className="flex items-center justify-between p-4 bg-white mb-2 rounded-lg shadow-sm"
            role="button"
            onClick={expandFilter}
          >
            <h2 className="ml-2 font-semibold text-xl">E-liquids</h2>
            <div className="flex items-center justify-center w-9 h-9 bg-gray-200 rounded-full text-gray-600">
              <FilterIcon width={21} height={21} />
            </div>
          </div>
        </div>
        <div
          className={cn('col-span-12 md:col-span-3', {
            'bg-white rounded-md': productFiltersLoading,
            'hidden md:block': !filterExpanded,
          })}
        >
          {productFilters ? (
            <Filter
              filters={productFilters[filterQuery.name]}
              updateFilters={updateFilters}
              priceRange={priceRange}
              updatePriceRange={updatePriceRange}
              selectedFilters={(filters.length > 0 && filters) || null}
            />
          ) : null}
        </div>
        <div className="col-span-12 md:col-span-9">
          {productsLoading ? (
            <Grid layout="normal" className="gap-4">
              {rangeMap(12, (i) => (
                <Skeleton
                  key={i}
                  className="w-full animated fadeIn h-64"
                  height={325}
                />
              ))}
            </Grid>
          ) : null}
          {data && !productsLoading ? (
            <Grid layout="normal" className="gap-4">
              {data.products.edges.map(({ node }: any) => (
                <Card
                  variant="simple"
                  key={node.path}
                  className="animated fadeIn"
                  product={node}
                  imgWidth={240}
                  imgHeight={240}
                />
              ))}
            </Grid>
          ) : null}
          {data ? (
            <Pagination
              total={data.products.pageInfo.totalPages}
              routingParams={routingParams}
              updatePage={fetchMore}
              className="mt-6"
            />
          ) : null}
        </div>
      </div>
    </Container>
  )
}
