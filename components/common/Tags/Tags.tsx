import { FC } from 'react'
import cn from 'classnames'

import s from './Tags.module.css'
import { Cross } from '@components/icons'

interface Props {
  tags: string[]
  variant: 'products-grid'
  title?: string
}

const Tags: FC<Props> = ({ tags, variant, title }) => (
  <div
    className={cn({
      [s.productTags]: variant === 'products-grid',
    })}
  >
    <ul className={s.tags}>
      {tags.map((t: string) => (
        <li>
          <button className={s.tag}>
            {t} <Cross width={16} height={16} className={s.cross} />
          </button>
        </li>
      ))}
    </ul>
  </div>
)

export default Tags
