import { FC } from 'react'
import cn from 'classnames'
import useCustomer from '@bigcommerce/storefront-data-hooks/use-customer'
import { Menu } from '@headlessui/react'

import { Cart } from '@components/icons'
import { Avatar } from '@components/common'
import { useUI } from '@components/ui/context'

import DropdownMenu from './DropdownMenu'
import s from './UserNav.module.css'

const UserNav: FC = () => {
  const { cartCount, toggleSidebar, openModal, setModalView } = useUI()
  const { data: customer } = useCustomer()
  const cartCountLS =
    typeof window !== 'undefined'
      ? ((window.localStorage.getItem('itemsCount') as unknown) as number)
      : 0
  const itemsCount = cartCountLS && cartCount <= 0 ? cartCountLS : cartCount

  return (
    <nav className={cn(s.root)}>
      <div className={s.mainContainer}>
        <ul className={s.list}>
          <li className={s.item}>
            <button onClick={toggleSidebar}>
              <Cart />
              {(itemsCount ?? 0) > 0 && (
                <span className={s.bagCount}>{itemsCount}</span>
              )}
            </button>
          </li>
          <li className={s.item}>
            {customer ? (
              <Menu>
                {({ open }) => (
                  <>
                    <Menu.Button className={s.avatarButton} aria-label="Menu">
                      <Avatar />
                    </Menu.Button>
                    <DropdownMenu open={open} />
                  </>
                )}
              </Menu>
            ) : (
              <button
                className={s.avatarButton}
                aria-label="Menu"
                onClick={() => {
                  setModalView('LOGIN_VIEW')
                  openModal()
                }}
              >
                <Avatar />
              </button>
            )}
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default UserNav
