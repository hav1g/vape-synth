import { FC } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import Image from 'next/image'

import { ProductNode } from '@lib/types/product'
import { usePrice } from '@lib/hooks/usePrice'
import s from './Card.module.css'

type Variant = 'slim' | 'simple' | 'insert'

type ImageLayout = 'fixed' | 'intrinsic' | 'responsive' | undefined

type ImageLoading = 'eager' | 'lazy'

interface Props {
  variant: Variant
  className?: string
  product: ProductNode
  imgWidth: number | string
  imgHeight: number | string
  imgPriority?: boolean
  imgSizes?: string
  imgLayout?: ImageLayout
  imgLoading?: ImageLoading
}

const Card: FC<Props> = ({
  variant,
  className,
  product: p,
  imgWidth,
  imgHeight,
  imgSizes,
  imgLayout,
  imgLoading,
  imgPriority,
}) => {
  const { price, basePrice, discount } = usePrice({
    amount: p.prices.price,
    baseAmount: p.prices?.retailPrice,
  })
  const src = p.images?.[0].urlStandard

  if (variant === 'insert') {
    return (
      <div
        className={s.insert}
        style={{
          backgroundImage: `linear-gradient(to right, rgba(99, 102, 241, 0.7), rgba(99, 102, 241, 0.7)), url('${src}')`,
        }}
      >
        <Link href={{ pathname: `/product${p.path}`, query: { id: p.id } }}>
          <a className="flex flex-col justify-center h-full px-6">
            <div className="w-3/4 md:w-2/4">
              <p className="text-3xl font-bold text-white">{p.name}</p>
            </div>
          </a>
        </Link>
      </div>
    )
  }

  return (
    <div
      className={cn(s.root, className, 'flex flex-col', {
        [s.simpleCard]: variant === 'simple',
      })}
    >
      <Link
        href={{
          pathname: `/product${p.path}`,
          query: { id: p.id },
        }}
      >
        <a>
          <div
            className={cn('flex justify-center py-2', {
              [s.slimCard]: variant === 'slim',
            })}
          >
            {discount ? (
              <div
                className={cn(s.discountBadge, {
                  [s['discountBadge--slim']]: variant === 'slim',
                  [s['discountBadge--simple']]: variant === 'simple',
                })}
              >
                <p className="font-semibold text-primary">-{discount}</p>
              </div>
            ) : null}
            <Image
              quality="85"
              width={imgWidth}
              sizes={imgSizes}
              height={imgHeight}
              layout={imgLayout}
              loading={imgLoading}
              priority={imgPriority}
              src={src}
              alt={p.images?.[0].description || 'Product Image'}
            />
          </div>
          <div
            className={cn('flex flex-col h-16 items-start', {
              'justify-center px-3 h-20 pb-2': variant === 'slim',
              'justify-between h-16 py-2': variant === 'simple',
            })}
          >
            <p className="text-center text-xl font-semibold">{p.name}</p>
            <div className="flex items-baseline">
              {basePrice ? (
                <p className="order-2 text-gray-400 text-sm line-through">
                  {basePrice}
                </p>
              ) : null}
              <p className="text-gray-700 text-md text-center order-1 mr-2">
                {price}
              </p>
            </div>
          </div>
        </a>
      </Link>
    </div>
  )
}

export default Card
