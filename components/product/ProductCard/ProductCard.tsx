import { FC, useState } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import Image from 'next/image'
import usePrice from '@bigcommerce/storefront-data-hooks/use-price'
import useAddItem from '@bigcommerce/storefront-data-hooks/cart/use-add-item'

import { Button } from '@components/ui'
import { Cart } from '@components/icons'
import { useUI } from '@components/ui/context'
import { API } from '@lib/constants'
import { ProductAlt, Pricing } from '@lib/types/product'
import s from './ProductCard.module.css'

interface Props {
  className?: string
  imgHeight: number | string
  imgLayout?: 'fixed' | 'intrinsic' | 'responsive' | undefined
  imgLoading?: 'eager' | 'lazy'
  imgPriority?: boolean
  imgSizes?: string
  imgWidth: number | string
  product: ProductAlt
  variant?: 'slim' | 'simple' | 'insert'
}

const ProductCard: FC<Props> = ({
  className,
  imgHeight,
  imgLayout,
  imgLoading,
  imgPriority,
  imgSizes,
  imgWidth,
  product: p,
  variant,
}) => {
  const addItem = useAddItem()
  const [loading, setLoading] = useState(false)
  const { openSidebar } = useUI()
  const isGql: boolean = p.source === API.BC_GQL
  const src = isGql
    ? p.images[0]?.urlStandard ?? ''
    : p.images.edges?.[0]?.node?.urlOriginal!
  const { price, basePrice, discount } = usePrice({
    amount: isGql ? p.price : p.prices?.price?.value,
    baseAmount: isGql ? p.retailPrice : p.prices?.retailPrice?.value,
    currencyCode: 'USD',
  }) as Pricing
  const addToCart = async () => {
    setLoading(true)
    try {
      await addItem({
        productId: p.entityId,
        variantId: p.variants.edges?.[0]?.node.entityId!,
      })
      openSidebar()
      setLoading(false)
    } catch (err) {
      setLoading(false)
    }
  }

  if (variant === 'insert') {
    return (
      <div
        className="bg-local h-full rounded-xl transition ease-in-out duration-150 shadow-sm hover:shadow-xl"
        style={{
          backgroundImage: `linear-gradient(to right, rgba(99, 102, 241, 0.7), rgba(99, 102, 241, 0.7)), url('${src}')`,
        }}
      >
        <Link href={`/product${isGql ? p.customUrl?.url : p.path}`}>
          <a className="flex flex-col justify-center h-full px-6">
            <div className="w-3/4 md:w-2/4">
              <p className="text-3xl font-bold text-white">{p.name}</p>
            </div>
          </a>
        </Link>
      </div>
    )
  }

  return (
    <div
      className={cn(s.root, className, 'flex flex-col', {
        [s.fullCard]: variant === 'simple',
      })}
    >
      <Link href={`/product${isGql ? p.customUrl?.url : p.path}`}>
        <a>
          <div
            className={cn('flex justify-center py-2', {
              [s.slimCard]: variant === 'slim',
            })}
          >
            {discount ? (
              <div className={s.discountBadge}>
                <p className="font-semibold text-primary">-{discount}</p>
              </div>
            ) : null}
            <div className="p-2">
              <Image
                quality="85"
                width={imgWidth}
                sizes={imgSizes}
                height={imgHeight}
                layout={imgLayout}
                loading={imgLoading}
                priority={imgPriority}
                src={src}
                alt={p.images.edges?.[0]?.node.altText || 'Product Image'}
              />
            </div>
          </div>
          <div className="flex items-center justify-center h-15 py-2">
            <p className="text-center">{p.name}</p>
          </div>
        </a>
      </Link>
      {variant === 'simple' ? (
        <div className="flex flex-col items-center justify-center">
          <div className="flex items-center justify-between">
            {basePrice ? (
              <p className="text-accents-4 line-through mr-4">{basePrice}</p>
            ) : null}
            <p className="text-2xl font-semibold text-center my-2">{price}</p>
          </div>
          <Button
            variant="slim"
            className="rounded-full"
            loading={loading}
            onClick={addToCart}
          >
            <span>Add to cart</span>
          </Button>
        </div>
      ) : null}
      {variant === 'slim' ? (
        <div className="flex items-center justify-between pt-4 h-16">
          <div className="flex flex-col items-start">
            <p className="text-center text-lg font-semibold">{price}</p>
            {basePrice ? (
              <p className="text-center line-through text-accents-4">
                {basePrice}
              </p>
            ) : null}
          </div>
          <div className="flex items-center">
            <Button variant="round" loading={loading} onClick={addToCart}>
              <Cart width={24} height={24} />
            </Button>
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default ProductCard
