import { useQuery } from '@apollo/client'
import upperFirst from 'lodash.upperfirst'
import { FC, useState } from 'react'
import cn from 'classnames'
import Image from 'next/image'
import { NextSeo } from 'next-seo'

import { Card } from '@components/product'
import { Cart } from '@components/icons'
import { Button, Container } from '@components/ui'
import { Select } from '@components/common'

import { usePrice } from '@lib/hooks/usePrice'
import { useCart } from '@lib/hooks/useCart'
import { ProductNode } from '@lib/types/product'
import { RELATED_PRODUCTS } from '@lib/queries'
import { UpdateOperation } from '@lib/constants'

import s from './ProductView.module.css'

type ImageIndex = number

interface Props {
  className?: string
  children?: any
  product: ProductNode
}

const ProductView: FC<Props> = ({ product }) => {
  const { createOrUpdateCart } = useCart()
  const { data } = useQuery(RELATED_PRODUCTS, {
    variables: { id: product.categories[0], limit: 5 },
  })
  const { price } = usePrice({
    amount: product.prices?.price,
    baseAmount: product.prices?.retailPrice,
  })
  const [variant, setVariant] = useState<{
    name: string
    id: number
  } | null>({
    name: (product.variants?.values[0].name as string) ?? '',
    id: (product.variants?.values[0].id as number) ?? null,
  })
  const [activeImage, setActiveImage] = useState<ImageIndex>(0)

  function onAddToCart() {
    const cartId =
      typeof window !== 'undefined' ? window.localStorage.getItem('cartId') : ''

    if (!cartId) {
      createOrUpdateCart({
        lineItems: [
          {
            product_id: product.id,
            variant_id: variant?.id,
            quantity: 1,
          },
        ],
      })

      return
    }

    createOrUpdateCart({
      operation: UpdateOperation.UPDATE,
      lineItems: [
        {
          product_id: product.id,
          variant_id: variant?.id,
          quantity: 1,
        },
      ],
    })
  }

  return (
    <Container className="max-w-none w-full" clean>
      <NextSeo
        title={product.name}
        description={product.description}
        openGraph={{
          type: 'website',
          title: product.name,
          description: product.description,
          images: [
            {
              url: product.images?.[0]?.urlStandard,
              width: 800,
              height: 600,
              alt: product.name,
            },
          ],
        }}
      />
      <div className="container mx-auto md:mt-14">
        <div className="grid grid-cols-12 bg-white gap-4 rounded-md shadow-md p-4">
          <div className="flex order-2 md:order-1 col-span-12 md:flex-col md:col-span-1 md:pl-4 md:py-4">
            {product.images.map((image, index: ImageIndex) => (
              <div
                key={image?.urlStandard}
                role="button"
                onClick={() => setActiveImage(index)}
                className={cn('mb-4 cursor-pointer', {
                  'opacity-25 hover:opacity-75 transition ease-in-out duration-150':
                    index !== activeImage,
                  'opacity-100': index === activeImage,
                })}
              >
                <Image
                  className={s.thumbnail}
                  src={image?.urlStandard ?? ''}
                  alt={image?.description || 'Product Image'}
                  width={72}
                  height={72}
                  quality="85"
                />
              </div>
            ))}
          </div>
          <div className="flex order-1 md:order-2 col-span-12 md:col-span-5 py-4">
            <Image
              className={s.img}
              src={product.images?.[activeImage]?.urlStandard ?? ''}
              alt={
                product.images?.[activeImage]?.description ?? 'Product Image'
              }
              width={1050}
              height={1050}
              quality="85"
            />
          </div>
          <div className="flex flex-col order-3 col-span-12 md:col-span-6 py-4 pr-4">
            <h1 className="text-3xl font-semibold">{product.name}</h1>
            <h2 className="text-2xl">{price}</h2>
            <div className="flex flex-col">
              <div
                className="py-6 mt-6 border-t-2 border-gray-100 text-lg"
                dangerouslySetInnerHTML={{ __html: product.description }}
              />
              {(product.variants?.values || []).length > 1 ? (
                // TODO: This check is super faggoty, but it's still possible to
                // have a variant even if a product doesn't have variants.
                // Should address this upstream in BFF.
                <Select
                  title={upperFirst(product.variants?.displayName)}
                  variants={product.variants?.values ?? []}
                  setChoice={({ name, id }) => setVariant({ name, id })}
                />
              ) : null}

              <div className="flex-grow-0 mt-12">
                <Button
                  aria-label="Add to Cart"
                  type="button"
                  onClick={onAddToCart}
                >
                  <Cart />
                  <span className="ml-2 normal-case text-lg">Add to cart</span>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {data ? (
        <div className="container m-auto py-12 pl-4 md:pl-0">
          <div className="flex flex-col justify-center">
            <h2 className="text-3xl font-semibold">Related products</h2>
            <div className="w-10 h-1 bg-red-500" />
          </div>
          <div className="flex mt-6 overflow-x-auto">
            {data.relatedProducts.edges.map(({ node }: any, idx: number) => (
              <Card
                variant="slim"
                key={`e:${node.id}--n:${node.name}`}
                className={cn('animated fadeIn', {
                  'rounded-tl-xl rounded-bl-xl': idx === 0,
                  'border-solid border-gray-100 border-l-2': idx > 0,
                  'rounded-tr-xl rounded-br-xl':
                    idx + 1 === data.relatedProducts.edges.length,
                })}
                product={node}
                imgWidth={240}
                imgHeight={240}
              />
            ))}
          </div>
        </div>
      ) : null}
    </Container>
  )
}

export default ProductView
