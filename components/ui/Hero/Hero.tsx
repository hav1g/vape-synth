import { FC, ReactElement } from 'react'

import { Container } from '@components/ui'
import s from './Hero.module.css'

interface Props {
  className?: string
  headline: ReactElement | null
  description?: ReactElement
  img?: ReactElement | null
  link?: ReactElement | null
}

const Hero: FC<Props> = ({ headline, description, className, img, link }) => {
  return (
    <div className={className}>
      <Container>
        <div className={s.root}>
          <div className="flex flex-col justify-center">
            {headline || null}
            {description || null}
            {link || null}
          </div>
          {img ? (
            <div className="flex flex-col items-end justify-center">{img}</div>
          ) : null}
        </div>
      </Container>
    </div>
  )
}

export default Hero
