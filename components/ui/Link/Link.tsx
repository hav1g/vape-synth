import cn from 'classnames'
import NextLink, { LinkProps as NextLinkProps } from 'next/link'

import s from './Link.module.css'

interface Props {
  variant?: 'large' | 'slim' | 'normal'
}

const Link: React.FC<NextLinkProps & Props> = ({
  href,
  variant,
  children,
  ...props
}) => {
  const classNames = cn({
    [s.largeRed]: variant === 'large',
  })

  return (
    <NextLink href={href}>
      <a className={classNames} {...props}>
        {children}
      </a>
    </NextLink>
  )
}

export default Link
