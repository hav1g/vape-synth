import { FC } from 'react'
import { useRouter } from 'next/router'
import cn from 'classnames'

import { ROUTING_PARAMS } from '@lib/constants'
import rangeMap from '@lib/range-map'
import s from './Pagination.module.css'

interface Props {
  routingParams: { [k: string]: string | number }
  total: number
  className?: string
  updatePage: (obj: { variables: { [k: string]: string | number } }) => void
}
const Pagination: FC<Props> = ({
  routingParams,
  total,
  className,
  updatePage,
}) => {
  const currentPath = routingParams?.[ROUTING_PARAMS.PATH_NAME] as string
  const router = useRouter()

  const handleClick = async (e: any, i: number) => {
    e.preventDefault()

    updatePage({
      variables: {
        offset: i * 12,
        limit: 12,
      },
    })

    await router.push({
      pathname: currentPath,
      query: { page: i + 1 },
    })

    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0)
    }
  }

  return (
    <ul className={cn('flex', className)}>
      {rangeMap(total, (i) => (
        //i is zero padded, therefore page 1 is i+1
        //which means page query param = i + 1
        <li key={`d--${i + 1}`} className="mr-2">
          <button className={s.root} onClick={(e) => handleClick(e, i)}>
            {i + 1}
          </button>
        </li>
      ))}
    </ul>
  )
}

export default Pagination
