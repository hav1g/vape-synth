import { Button, Modal } from '@components/ui'
import { useIamNotAChild } from '@lib/hooks/useIAmNotAChild'
import React from 'react'

const AgeVerification: React.FC = () => {
  const { onIAmNotAChildConfirmation } = useIamNotAChild()

  return (
    <div className="w-96 flex justify-center flex-col">
      <p className="mt-3 mb-3">
        The products available on Synth Vape are age-restricted and intended for
        adults of legal smoking age only. All orders placed on the website will
        be verified by an industry leading Age Verification software for
        validation. By entering our website, you affirm that you are of legal
        smoking age in your jurisdiction and you agree to be Age Verified.
      </p>
      <Button
        aria-label="I agree"
        className="m-2"
        onClick={() => onIAmNotAChildConfirmation(true)}
      >
        I am over 21
      </Button>
      <Button
        aria-label="I agree"
        className="m-2"
        onClick={() => onIAmNotAChildConfirmation(false)}
        style={{ backgroundColor: 'transparent', color: 'black' }}
      >
        I am under 21
      </Button>
    </div>
  )
}

export default AgeVerification
