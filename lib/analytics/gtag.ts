export const GA_TRACKING_ID = 'G-K8YXSXXH3H' // This is your GA Tracking ID

declare global {
  interface Window {
    gtag(arg1: string, arg2: string, arg3: Object): void
    datalayer: []
  }
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url: string) => {
  if (typeof window !== 'undefined') {
    window.gtag('config', GA_TRACKING_ID, {
      page_location: url,
    })
  }
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }: any) => {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value,
  })
}
