export enum Categories {
  E_LIQUIDS = 71,
  SHOP_ALL = 23,
  VAPE_PARTS = 30,
  VAPES = 24,
  ACCESSORIES = 73,
  COILS = 74,
  MODS = 72,
  FEATURED = 75,
  BATTERIES = 76,
}

export enum UpdateOperation {
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}

export enum CartStatus {
  DELETED = 'DELETED',
}

export enum StatusCodes {
  MISSING_OR_INVALID_DATA = 422,
  NOT_FOUND = 404,
}

export const E_LIQUDS_BLACK_LIST = [
  Categories.SHOP_ALL,
  Categories.VAPES,
  Categories.VAPE_PARTS,
  Categories.E_LIQUIDS,
  Categories.MODS,
  Categories.ACCESSORIES,
  Categories.COILS,
  Categories.FEATURED,
  Categories.BATTERIES,
]

// TODO: Delete this after removing `ProductCard` in favor of `Card` on `/search`
export enum API {
  BC_GQL = 'bc-gql',
}

export const DEFAULT_RANGE = { min: 0, max: 500 }

export const ROUTING_PARAMS = {
  PATH_NAME: 'pathname',
  CURRENT_PAGE_NUMBER: 'currentPageNumber',
}
