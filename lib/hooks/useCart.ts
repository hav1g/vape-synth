import { useLazyQuery, useMutation } from '@apollo/client'
import { useEffect, useState } from 'react'
import dayjs from 'dayjs'

import { useUI } from '@components/ui/context'
import { CREATE_CART, UPDATE_CART, UPDATE_ITEM_IN_CART } from '@lib/mutations'
import { GET_CART } from '@lib/queries'
import { CartStatus, UpdateOperation } from '@lib/constants'

export function useCart() {
  const { setCartCount } = useUI()

  const [
    getCart,
    { data: existingCart, loading: existingCartLoading },
  ] = useLazyQuery(GET_CART)

  const [createCart, { data: newCart, loading: newCartLoading }] = useMutation(
    CREATE_CART,
  )

  const [
    updateCart,
    { data: updatedCart, loading: updatedCartLoading },
  ] = useMutation(UPDATE_CART)

  const [
    updateItemInCart,
    { data: updatedItems, loading: updatedItemsLoading },
  ] = useMutation(UPDATE_ITEM_IN_CART)

  const [cart, setCart] = useState<Cart | null>(() => {
    const cartId =
      typeof window !== 'undefined' ? window.localStorage.getItem('cartId') : ''

    if (cartId && window.sessionStorage.getItem('cart')) {
      return {
        ...JSON.parse(window.sessionStorage.getItem('cart') as string),
      }
    }

    return null
  })

  const [errors, setErrors] = useState<StatusError[]>([])

  function getItemsCount(items: CartItem[]) {
    return items.reduce((total, { quantity }) => total + quantity, 0)
  }

  function createOrUpdateCart({
    operation,
    lineItems,
  }: {
    operation?: Operation
    lineItems?: LineItem[]
  }) {
    const cartId =
      typeof window !== 'undefined' ? window.localStorage.getItem('cartId') : ''

    if (
      cartId &&
      (operation === UpdateOperation.UPDATE ||
        operation === UpdateOperation.DELETE)
    ) {
      updateCart({ variables: { cartId, lineItems, operation } })

      return
    }

    createCart({ variables: { lineItems } })
  }

  function updateItem(lineItem: Partial<CartItem>, operation: Operation) {
    const cartId =
      typeof window !== 'undefined' ? window.localStorage.getItem('cartId') : ''

    updateItemInCart({
      variables: { cartId, lineItem, operation },
    })
  }

  function updateCartStorage(cart: Cart) {
    window.sessionStorage.setItem('cart', JSON.stringify({ ...cart }))

    setCart({ ...cart })

    setCartCount(getItemsCount(cart.items))

    window.localStorage.setItem(
      'itemsCount',
      (getItemsCount(cart.items) as unknown) as string,
    )
  }

  function resetCartStorage() {
    const localStorageKeys = ['cartId', 'itemsCount', 'expires_at']
    const sessionStorageKeys = ['cart']

    for (const key of localStorageKeys) {
      window.localStorage.removeItem(key)
    }

    for (const key of sessionStorageKeys) {
      window.sessionStorage.removeItem(key)
    }

    setCartCount(0)
    setCart(null)
  }

  useEffect(() => {
    const cartId = window.localStorage.getItem('cartId')

    // Someone already has a cart so request the full contents of the cart
    // based off of the `cartId` that's stored in local storage
    if (cartId && !window.sessionStorage.getItem('cart')) {
      getCart({ variables: { cartId } })
    }
  }, [])

  useEffect(() => {
    // Update `cart` session storage on mount
    if (existingCart?.getCart?.id && !existingCartLoading) {
      updateCartStorage(existingCart.getCart)
    }

    // Creating a `cart` for the first time
    if (
      newCart?.createCart?.id &&
      !window.localStorage.getItem('cartId') &&
      !newCartLoading
    ) {
      window.localStorage.setItem('cartId', newCart.createCart.id)

      if (!window.localStorage.getItem('expires_at')) {
        window.localStorage.setItem(
          'expires_at',
          `${dayjs(Date.now()).add(1, 'day')}`,
        )
      }

      updateCartStorage(newCart.createCart)
    }

    // Adding a new item to a `cart`
    if (
      updatedCart?.updateCart?.id &&
      !newCart?.createCart?.id &&
      !updatedCart.updateCart?.status &&
      !updatedCartLoading
    ) {
      updateCartStorage(updatedCart.updateCart)
    }

    // After deleting a cart, `bc-graphql` responds with a
    // `{ status: 'DELETED' }` so reset everything
    if (
      // updatedCart?.updateCart?.status &&
      updatedCart?.updateCart?.status === CartStatus.DELETED &&
      !updatedCartLoading
    ) {
      resetCartStorage()
    }

    // Updating the items in a `cart` e.g. updating the quantity
    // TODO: Should consider combining `status` and `error` since they basically do the same thing
    if (
      updatedItems?.updateItemInCart?.id &&
      !updatedItems.updateItemInCart?.status &&
      !updatedItems.updateItemInCart?.error &&
      !updatedItemsLoading
    ) {
      updateCartStorage(updatedItems.updateItemInCart)
    }

    // // Updating the items in a `cart`, but the qty is greater than the inventory
    if (updatedItems?.updateItemInCart?.error && !updatedItemsLoading) {
      setErrors([
        ...errors,
        {
          code: updatedItems.updateItemInCart.error.code,
          title: updatedItems.updateItemInCart.error.title,
        },
      ])
    }

    // After removing the last item, `bc-graphql` responds with a
    // `{ status: 'DELETED' }` so reset everything
    if (
      // updatedItems &&
      updatedItems?.updateItemInCart?.status === CartStatus.DELETED &&
      !updatedItems.updateItemInCart?.error &&
      !updatedItemsLoading
    ) {
      resetCartStorage()
    }
  }, [
    newCart?.createCart?.id,
    existingCart?.getCart?.id,
    updatedCart?.updateCart?.total,
    updatedCart?.updateCart?.status,
    updatedItems?.updateItemInCart?.total,
    updatedItems?.updateItemInCart?.status,
    updatedItems?.updateItemInCart?.error,
  ])

  return {
    createOrUpdateCart,
    updateItem,
    cart,
    errors,
    resetCartStorage,
  }
}
