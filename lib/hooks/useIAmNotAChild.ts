import { useUI } from '@components/ui/context'
import Cookies from 'js-cookie'

const COOKIE_NAME = 'not_a_child'

interface IAmNotAChildHook {
  onIAmNotAChildConfirmation: (arg: boolean) => void
  displayAgeVerificationModal: () => void
}

export const useIamNotAChild = (): IAmNotAChildHook => {
  const { setModalView, openModal, closeModal } = useUI()

  const displayAgeVerificationModal = (): void => {
    if (!Cookies.get(COOKIE_NAME)) {
      setModalView('AGE_VERIFICATION')
      openModal()
    }
  }

  const confirmNotAChild = (result: boolean): void => {
    if (!result) {
      // not sure where else to go
      // history.back will just cause them to close the modal
      window.location.href = 'https://www.google.com'
      return
    }

    Cookies.set(COOKIE_NAME, `${result}`)
    closeModal()
  }

  return {
    onIAmNotAChildConfirmation: confirmNotAChild,
    displayAgeVerificationModal,
  }
}
