import { useMemo } from 'react'

function formatPrice({ amount }: { amount: number }) {
  const formatCurrency = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  })

  return formatCurrency.format(amount)
}

export function usePrice({ amount, baseAmount }: { [k: string]: number }) {
  const isDiscounted = baseAmount > amount
  const formatDiscount = new Intl.NumberFormat('en-US', { style: 'percent' })

  const value = useMemo(() => {
    return {
      price: formatPrice({ amount }),
      basePrice: isDiscounted ? formatPrice({ amount: baseAmount }) : null,
      discount: isDiscounted
        ? formatDiscount.format((baseAmount - amount) / baseAmount)
        : null,
    }
  }, [amount, baseAmount])

  return value
}
