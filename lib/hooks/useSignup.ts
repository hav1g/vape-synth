import { useMutation } from '@apollo/client'
import { useEffect } from 'react'

import { CREATE_ACCOUNT } from '@lib/mutations'

interface Account {
  [k: string]: string
}

export function useSignup() {
  const [createAccount, { data, loading }] = useMutation(CREATE_ACCOUNT)

  async function signup({ lastName, firstName, email, password }: Account) {
    createAccount({
      variables: {
        account: {
          last_name: lastName,
          first_name: firstName,
          email,
          authentication: { new_password: password },
        },
      },
    })
  }

  useEffect(() => {
    if (data && !loading) {
      console.log(data)
    }
  }, [data, loading])

  return { signup }
}
