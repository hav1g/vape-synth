import { gql } from '@apollo/client'

export const CREATE_CART = gql`
  mutation CreateCart($lineItems: [Item!]) {
    createCart(lineItems: $lineItems) {
      id
      subTotal
      total
      items {
        id
        variantId
        productId
        quantity
        name
        variantName
        imageUrl
        extendedListPrice
        extendedSalePrice
        listPrice
        salePrice
        sku
      }
    }
  }
`

export const UPDATE_CART = gql`
  mutation UpdateCart(
    $cartId: String!
    $lineItems: [Item]
    $operation: UpdateOperation!
  ) {
    updateCart(cartId: $cartId, lineItems: $lineItems, operation: $operation) {
      id
      subTotal
      total
      items {
        id
        variantId
        productId
        quantity
        name
        variantName
        imageUrl
        extendedListPrice
        extendedSalePrice
        listPrice
        salePrice
        sku
      }
      status
    }
  }
`

export const UPDATE_ITEM_IN_CART = gql`
  mutation UpdateItemInCart(
    $cartId: String!
    $lineItem: Item
    $operation: UpdateOperation!
  ) {
    updateItemInCart(
      cartId: $cartId
      lineItem: $lineItem
      operation: $operation
    ) {
      id
      subTotal
      total
      items {
        id
        variantId
        productId
        quantity
        name
        variantName
        imageUrl
        extendedListPrice
        extendedSalePrice
        listPrice
        salePrice
        sku
      }
      status
      error {
        code
        title
      }
    }
  }
`

export const CREATE_ACCOUNT = gql`
  mutation CreateAccount($account: CreateAccount) {
    createAccount(account: $account) {
      email
      id
    }
  }
`

export const CREATE_CHECKOUT_REDIRECTS = gql`
  mutation($cartId: String!) {
    createCheckoutRedirects(cartId: $cartId) {
      checkoutUrl
    }
  }
`
