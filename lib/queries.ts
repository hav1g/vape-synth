import { gql } from '@apollo/client'

export const ELIQUIDS_FILTER = gql`
  query ELiquidFilters {
    eLiquidFilters {
      displayName
      filters {
        id
        name
      }
    }
  }
`

export const FRONT_PAGE = gql`
  query FrontPage {
    frontPage {
      featured {
        edges {
          node {
            id
            name
            prices {
              price
              salePrice
              retailPrice
            }
            images {
              urlStandard
              description
              id
            }
            path
          }
        }
      }
      newest {
        edges {
          node {
            id
            name
            prices {
              price
              salePrice
              retailPrice
            }
            images {
              urlStandard
              description
              id
            }
            path
          }
        }
      }
    }
  }
`

export const PRODUCTS = gql`
  query AllProducts(
    $categoriesIn: [Int]
    $price: FilterPrice
    $offset: Int
    $limit: Int
    $type: String
  ) {
    products(
      categoriesIn: $categoriesIn
      price: $price
      offset: $offset
      limit: $limit
      type: $type
    ) {
      edges {
        node {
          availability
          brandId
          categories
          description
          id
          images {
            id
            description
            urlStandard
          }
          isInStock
          name
          path
          prices {
            price
            retailPrice
            salePrice
          }
        }
      }
      pageInfo {
        offset
        limit
        totalPages
      }
    }
  }
`

export const SINGLE_PRODUCT = gql`
  query Product($id: Int!) {
    product(id: $id) {
      edges {
        node {
          categories
          description
          id
          images {
            id
            description
            urlStandard
          }
          isInStock
          name
          inventoryLevel
          variants {
            displayName
            values {
              id
              name
              inventoryLevel
              inventoryWarningLevel
            }
          }
          path
          prices {
            price
            retailPrice
            salePrice
          }
        }
      }
    }
  }
`

export const RELATED_PRODUCTS = gql`
  query RelatedProducts($id: Int!, $limit: Int!) {
    relatedProducts(id: $id, limit: $limit) {
      edges {
        node {
          id
          name
          prices {
            price
            salePrice
            retailPrice
          }
          images {
            urlStandard
            description
            id
          }
          path
        }
      }
    }
  }
`

export const GET_CART = gql`
  query GetCart($cartId: String!) {
    getCart(cartId: $cartId) {
      id
      subTotal
      total
      items {
        id
        variantId
        productId
        quantity
        name
        variantName
        imageUrl
        extendedListPrice
        extendedSalePrice
        listPrice
        salePrice
        sku
      }
    }
  }
`

export const GET_BRANDS = gql`
  query GetBrands {
    brands {
      edges {
        node {
          id
          name
        }
      }
    }
  }
`
