type LineItem = {
  product_id: number
  variant_id: number | undefined
  quantity: number
}

interface CartItem {
  id: string
  variantId: number
  productId?: number
  product_id?: number // TODO: fix this gay shit so we only have one got dang product id property
  quantity: number
  name: string
  variantName: string
  extendedListPrice: number
  extendedSalePrice?: number
  listPrice?: number
  salePrice?: number
  imageUrl?: string
  sku?: string
}

interface Cart {
  id: string
  subTotal: number
  total: number
  items: CartItem[]
}

interface StatusError {
  code: number
  title: string
}

type Operation = 'UPDATE' | 'DELETE'
