import type { ProductNode } from '@bigcommerce/storefront-data-hooks/api/operations/get-all-products'

export type Pricing = {
  price: string
  basePrice: string | null
  discount: string | null
  retailPrice: string | null
}

export interface ProductAlt extends ProductNode {
  source?: string
  price?: string | number
  retailPrice?: string | number
  images: any
  customUrl?: { url: string }
}

export interface ProductNode {
  availability?: 'available' | 'preorder' | 'disabled'
  brandId?: number
  categories: number[]
  description?: any
  id: number
  images: Array<{
    id: number
    isThumbnail?: boolean
    description?: string
    urlStandard: string
    urlThumbnail?: string
  }>
  isInStock: boolean
  name: string
  path: string
  prices: { price: number; retailPrice: number; salePrice: number }
  sku?: string
  variants?: {
    displayName: string
    values: Array<{
      id: number
      name: string
      inventoryLevel: number
      inventoryWarningLevel: number
    }>
  }
}
