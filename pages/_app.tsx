import '@assets/main.css'
import 'rc-slider/assets/index.css'

import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'
import { FC, useEffect } from 'react'
import type { AppProps } from 'next/app'

import { ManagedUIContext } from '@components/ui/context'
import { Head } from '@components/common'
import * as gtag from '@lib/analytics/gtag'
import { useRouter } from 'next/router'
import { relayStylePagination } from '@apollo/client/utilities'

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          products: relayStylePagination(['type']),
        },
      },
      Cart: {
        fields: {
          item: {
            merge(existing = [], incoming) {
              if (!incoming) {
                return
              }

              return [...exisiting, incoming]
            },
          },
        },
      },
      Mutation: {
        fields: {
          updateCart: {
            fetchPolicy: 'no-cache',
          },
        },
      },
      LineItem: {
        merge: true,
      },
    },
  }),
})

const Noop: FC = ({ children }) => <>{children}</>

export default function MyApp({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout || Noop
  const router = useRouter()

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      gtag.pageview(url)
    }

    router.events.on('routeChangeComplete', handleRouteChange)

    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <ApolloProvider client={client}>
      <Head />
      <ManagedUIContext>
        <Layout pageProps={pageProps}>
          <Component {...pageProps} />
        </Layout>
      </ManagedUIContext>
    </ApolloProvider>
  )
}
