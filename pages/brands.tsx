import { useQuery } from '@apollo/client'

import { Layout } from '@components/common'
import { Container } from '@components/ui'
import { GET_BRANDS } from '@lib/queries'

export default function Brands() {
  const { data } = useQuery(GET_BRANDS)

  return (
    <Container>
      {data ? (
        <ul>
          {data.brands.edges.map(({ node }) => (
            <li key={node?.id}>{node?.name}</li>
          ))}
        </ul>
      ) : null}
    </Container>
  )
}

Brands.Layout = Layout
