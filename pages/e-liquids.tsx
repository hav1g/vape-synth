import { Categories } from '@lib/constants'
import { ELIQUIDS_FILTER } from '@lib/queries'
import { Layout, ProductListLayout } from '@components/common'

export default function ELiquids() {
  return (
    <ProductListLayout
      categoriesIn={[Categories.E_LIQUIDS]}
      type="E_LIQUIDS"
      filterQuery={{ q: ELIQUIDS_FILTER, name: 'eLiquidFilters' }}
    />
  )
}

ELiquids.Layout = Layout
