import { useQuery } from '@apollo/client'
import Image from 'next/image'
import cn from 'classnames'

import { Layout } from '@components/common'
import { Card } from '@components/product'
import { Link, Hero } from '@components/ui'
import { FRONT_PAGE } from '@lib/queries'

export default function Home() {
  const { data } = useQuery(FRONT_PAGE)

  const borders = (idx: number) => ({
    'rounded-tl-xl rounded-bl-xl': idx === 0,
    'border-solid border-gray-100 border-l-2': idx > 0,
    'rounded-tr-xl rounded-br-xl': idx + 1 === 6,
  })

  return (
    <div>
      <Hero
        headline={
          data ? (
            <h2 className="text-6xl leading-none font-extrabold text-white">
              {data.frontPage.newest.edges[1].node.name}
            </h2>
          ) : null
        }
        className="bg-gradient-to-r from-purple-500 to-red-500"
        img={
          data ? (
            <Image
              quality="85"
              width={489}
              height={411}
              layout="fixed"
              src={data.frontPage.newest.edges[1].node.images?.[0].urlStandard}
              alt={
                data.frontPage.newest.edges[1].node.images?.[0].description ||
                'Product Image'
              }
            />
          ) : null
        }
        link={
          data ? (
            <Link
              href={{
                pathname: `/product${data.frontPage.newest.edges[1].node.path}`,
                query: { id: data.frontPage.newest.edges[1].node.id },
              }}
              variant="large"
            >
              Shop now
            </Link>
          ) : null
        }
      />

      {data ? (
        <>
          <div className="container m-auto py-12 pl-4 md:pl-0">
            <div className="flex flex-col justify-center">
              <h2 className="text-3xl font-semibold">Featured products</h2>
              <div className="w-10 h-1 bg-red-500" />
            </div>
            <div className="flex mt-6 overflow-x-auto md:overflow-x-hidden">
              {data.frontPage.featured.edges.map(
                ({ node }: any, idx: number) => (
                  <Card
                    variant="slim"
                    key={node.path}
                    className={cn('animated fadeIn', borders(idx))}
                    product={node}
                    imgWidth={240}
                    imgHeight={240}
                  />
                ),
              )}
            </div>
          </div>

          <div className="container mx-auto px-4 md:px-0">
            <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
              <div className="h-64">
                <Card
                  variant="insert"
                  product={data.frontPage.featured.edges?.[0]?.node}
                  imgHeight={0}
                  imgWidth={0}
                />
              </div>
              <div className="h-64">
                <Card
                  variant="insert"
                  product={data.frontPage.featured.edges?.[1]?.node}
                  imgHeight={0}
                  imgWidth={0}
                />
              </div>
            </div>
          </div>

          <div className="container m-auto py-12 pl-4 md:pl-0">
            <div className="flex flex-col justify-center">
              <h2 className="text-3xl font-semibold">Newest products</h2>
              <div className="w-10 h-1 bg-red-500" />
            </div>
            <div className="flex mt-6 overflow-x-auto md:overflow-x-hidden">
              {data.frontPage.newest.edges.map(({ node }: any, idx: number) => (
                <Card
                  variant="slim"
                  key={node.path}
                  className={cn('animated fadeIn', borders(idx))}
                  product={node}
                  imgWidth={240}
                  imgHeight={240}
                />
              ))}
            </div>
          </div>
        </>
      ) : null}
    </div>
  )
}

Home.Layout = Layout
