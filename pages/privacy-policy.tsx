import { Layout } from '@components/common'
import { Container } from '@components/ui'

export default function PrivacyPolicy() {
  return (
    <Container>
      <h1>Privacy Policy</h1>
    </Container>
  )
}

PrivacyPolicy.Layout = Layout
