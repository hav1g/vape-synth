import { useRouter } from 'next/router'
import { Layout } from '@components/common'
import { ProductView } from '@components/product'

import { SINGLE_PRODUCT } from '@lib/queries'
import { useLazyQuery } from '@apollo/client'
import { useEffect } from 'react'

export default function Slug() {
  const router = useRouter()
  const [getProduct, { loading, data }] = useLazyQuery(SINGLE_PRODUCT)

  useEffect(() => {
    if (router.query.id) {
      getProduct({ variables: { id: parseInt(router.query.id as string, 10) } })
    }
  }, [router.query.id])

  return data && !loading ? (
    <ProductView product={data?.product.edges[0].node} />
  ) : null
}

Slug.Layout = Layout
