import { Layout } from '@components/common'
import { Container } from '@components/ui'

export default function ReturnsExchange() {
  return (
    <Container>
      <h1>Returns and exchanges</h1>
    </Container>
  )
}

ReturnsExchange.Layout = Layout
