import { Layout } from '@components/common'
import { Container } from '@components/ui'

export default function TermsOfUse() {
  return (
    <Container>
      <h1>Terms of use</h1>
    </Container>
  )
}

TermsOfUse.Layout = Layout
